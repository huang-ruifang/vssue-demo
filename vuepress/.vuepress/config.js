module.exports = {
  title: 'Vssue Demo',
  description: 'A Vue-powered Issue-based Comment Plugin',

  plugins: [
    ['@vssue/vuepress-plugin-vssue', {
      platform: 'gitee',
      owner: 'huang-rui',
      repo: 'vssue-demo',
      clientId: 'ddd2fac1da66d09993c8397313d394f7d798cc9b485f98fea85d382dae78f252',
      clientSecret: 'e70628b6b4fee73e98e346e9e34856b1ba40692405617862360e0363b8ccf15e',
    }],
  ],
}
